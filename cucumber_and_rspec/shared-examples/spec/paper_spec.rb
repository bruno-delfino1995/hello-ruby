require 'paper'
require 'shared_examples'

describe Paper do
  # The unique caution with shared examples <review>is doesn't</review> use too much, in order to keep
  # the tests clear
  it_behaves_like 'a publishable object'
end
