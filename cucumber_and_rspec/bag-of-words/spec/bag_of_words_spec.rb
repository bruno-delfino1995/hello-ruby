require 'bag_of_words'
require 'rspec/collection_matchers'

describe BagOfWords do
  it 'is possible to put words on it' do
    bag = BagOfWords.new

    bag.put 'hello', 'world'

    # We do by this way to the message of error be more descriptive, because hadn't
    # been fully clear with expect(bag.words.size).to eq(2)
    # We should make our tests be descriptives and have error messages clearer as possible
    expect(bag).to have(2).words
  end
end
