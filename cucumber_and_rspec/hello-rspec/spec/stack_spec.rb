class Stack
  def initialize
    @elements = []
  end

  def push(element)
    @elements << element
  end

  def top
    @elements[-1]
  end

  def pop
    @elements.pop
  end
end

# describe groups the specifications of determined context
describe Stack do
  describe '#push' do
    # it is the code to be executed as test, an example of how the code should work
    it 'puts an element at the top of the stack' do
      stack = Stack.new

      stack.push 1
      stack.push 2

      # Assertion used to check if the code has behaved as expected. Done as
      # expectations, the assertions have this structure:
      # expect(actual).to matcher(expected)
      expect(stack.top).to eq(2)
    end
  end

  describe '#pop' do
    it 'removes the element at the top of the stack' do
      # setup
      stack = Stack.new

      # execute
      stack.push 1
      stack.push 2

      # verify
      expect(stack.pop).to eq(2)
      expect(stack.top).to eq(1)
    end
  end
end


# Every test code following the xUnit model has 4 phases:
# setup: the preparation of the system to init the test
# execute: when you interact with the system
# verify: where you verigy the results of execution
# teardown: the cleanup after the test
