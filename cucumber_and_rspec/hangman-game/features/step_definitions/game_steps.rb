# encoding: UTF-8

# Here we use a helper function to decrease the duplication in test code, by definition
# helper funcions stay in support folder and are loaded by default
When /^começo um novo jogo$/ do
  start_new_game
end

Then /^vejo na tela:$/ do |string|
  # steps function is used to call one or more steps inside a step definition, thus
  # executing two or more steps instead one. steps function is different of the step
  # function, in step we pass the string that matches with regex in the step definition,
  # without the first word, in steps we pass the phrase including the word
  steps %Q{
    Then the output should contain "#{string}"
  }
end

# We could have used the same step, but the feature file has to be written with
# clarity, oriented to reading not for the development
Given /^que comecei um jogo$/ do
  start_new_game
end

When /^escolho que a palavra a ser sorteada deverá ter "(.*?)" letras$/ do
  |number_of_letters|
  steps %{
    When I type "#{number_of_letters}"
  }
end

Then /^o jogo termina com a seguinte mensagem na tela:$/ do |message|
  steps %{
    Then it should pass with:
      """
      #{message}
      """
  }
end

Then /^termino o jogo$/ do
  steps %{
    When I type "fim"
  }
end

Given /^o jogo tem as possíveis palavras para sortear:$/ do |words_table|
  words = words_table.rows.map(&:last).join(" ")
  set_raffable_words words
end

Given /^que escolhi que a palavra a ser sorteada deverá ter "(.*?)" letras$/ do
  |number_of_letters|
  steps %{
    When I type "#{number_of_letters}"
  }
end

When /^tento adivinhar que a palavra tem a letra "([^"]*)"$/ do |letter|
  steps %{
    When I type "#{letter}"
  }
end

Then /^o jogo mostra que adivinhei uma letra com sucesso$/ do
  steps %{
    Then the output should contain:
      """
      Você adivinhou uma letra com sucesso.
      """
  }
end

Then /^o jogo mostra errei a adivinhação da letra$/ do
  steps %{
    Then the output should contain:
      """
      Você errou a letra.
      """
  }
end

When /^tento adivinhar que a palavra tem a letra "([^"]*)" "([^"]*)" vezes$/ do
  |letter, times|
  times.to_i.times do
    steps %{
      * tento adivinhar que a palavra tem a letra "#{letter}"
    }
  end
end
