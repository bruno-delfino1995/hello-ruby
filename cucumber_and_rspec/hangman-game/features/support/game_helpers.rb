module GameHelpers
  def set_raffable_words(words)
    @raffable_words = words
  end

  def start_new_game
    @raffable_words ||= %w[hi mom game fruit];

    steps %{
      When I run `bin/forca "#{@raffable_words}"` interactively
    }
  end
end

# This method include the module methods in the step definitions
World GameHelpers
