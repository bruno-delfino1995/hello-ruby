class CreateTodos < ActiveRecord::Migration[5.0]
  def change
    create_table :todos do |t|
      t.string :name, limit: 50
      t.date :date
      t.string :description

      t.timestamps
    end
  end
end
