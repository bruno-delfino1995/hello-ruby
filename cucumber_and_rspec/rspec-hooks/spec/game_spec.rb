require 'game'

# Normally we group the tests by classes, but we can group using a class and
# a string describing the state of the class in the test
describe Game, 'int the final phase' do
  # Before is a method to execute this code before each or some test, or before
  # everything in this context, the default option is before each test
  before do
    @game = Game.new
    @game.phase = :final
  end

  # context is only an alias for describe, is used to improve the readability
  context 'when the player hits the target' do
    it 'congratulates the player' do
      @game.player_hits_target

      expect(@game.output).to eq('Congratulations!')
    end

    it 'sets the score to 100' do
      @game.player_hits_target

      expect(@game.score).to eq(100)
    end
  end
end

# When you are refactoring your tests prioritize the clarity of them, if using DRY
# you affect the readability of the test, analyze if is advantageous
# In test code, the most important characteristic is the clarity, and the possibility
# of easy understanding the relation of cause and consequence
