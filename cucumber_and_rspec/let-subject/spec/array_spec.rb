describe Array, 'with some elements' do
  # subject is used to define the SUT (system under test, the object to be tested)
  # it defines an method with this name that execute returns the value returned by
  # the block provided, therefore the method provided executes one time to configure
  # the return
  subject :array do [1, 2, 3] end

  it 'should have the prescribed elements' do
    expect(array()).to eq([1, 2, 3])
  end

  # We can use an before hook, however with subject we explicit the SUT more clearly
  # The difference between subject and a before hook, is that subject was made to
  # manipulate the SUT and the before hook to do the setup of the test
  before do
    @array = [1, 2, 3]
  end

  it 'should have the prescribed elements - using before' do
    expect(@array).to eq([1, 2, 3])
  end
end

describe 'The lazy-evaluated behavior of let' do
  before do @foo = 'bar' end

  # let is used to define an helper method with the content provided in the block,
  # so the code is only execute when called
  let (:broken_operation) do raise 'I\'m broken' end

  it 'will call the method defined by let' do
    expect {
      expect(@foo).to eq('bar')
      broken_operation
    }.to raise_error('I\'m broken')
  end

  it 'won\'t call the method defined by let' do
    expect {
      expect(@foo).to eq('bar')
    }.not_to raise_error
  end
end
